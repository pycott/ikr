# coding=utf-8
from __future__ import unicode_literals

from twisted.cred.portal import IRealm
from twisted.web.resource import IResource
from zope.interface import implements


class IKRRealm(object):
    implements(IRealm)

    def __init__(self, resource, factory):
        self.resource = resource
        self.factory = factory

    def get_username(self, avatarId):
        return avatarId.split('@', 1)[0]

    def requestAvatar(self, avatarId, mind, *interfaces):
        if IResource in interfaces:
            resource = self.resource(self.factory)
            resource.username = self.get_username(avatarId)
            return (IResource, resource, lambda: None)
        raise NotImplementedError("this interface not supported by this realm")
