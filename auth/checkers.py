# coding=utf-8
from __future__ import unicode_literals

from twisted.cred import checkers, error as credError
from twisted.cred.credentials import IUsernamePassword
from twisted.internet import defer
from zope.interface import implements


class DebugBasicAuthorizationChecker:
    implements(checkers.ICredentialsChecker)
    credentialInterfaces = (IUsernamePassword,)

    def __init__(self, users):
        self.users = users

    def requestAvatarId(self, credentials):
        username = credentials.username
        if self.users.has_key(username):
            if credentials.password == self.users[username]:
                return defer.succeed(username)
            else:
                return defer.fail(credError.UnauthorizedLogin("Bad password"))
        else:
            return defer.fail(credError.UnauthorizedLogin("No such user"))
