# coding=utf-8

from twisted.application import internet
from twisted.application import service
from twisted.internet import ssl
from twisted.plugin import IPlugin
from twisted.python import usage
from twisted.web.server import Site
from zope.interface import implements

from server.api import IKRApi
from settings import main as settings


class Options(usage.Options):
    optParameters = [
        ['ws_port', None, settings.WS_PORT, 'The port number to listen on websocket.'],
        ['wss_port', None, settings.WSS_PORT, 'The port number to listen on secure websocket.'],
        ['iface', 'i', settings.INTERFACE, 'The interface to listen on.'],
    ]


class IKRServiceMaker(object):
    implements(service.IServiceMaker, IPlugin)

    tapname = "ikr"
    description = "A RBT communicator service."
    options = Options

    def makeService(self, options):
        top_service = service.MultiService()
        site = Site(IKRApi())
        if settings.USE_WS:
            ws_port = int(options['ws_port'])
            ikr_service = internet.TCPServer(ws_port, site, interface=options['iface'])
            ikr_service.setServiceParent(top_service)
        if settings.USE_WSS:
            wss_port = int(options['wss_port'])
            contextFactory = ssl.DefaultOpenSSLContextFactory(settings.SSL_PRIVATE_CRT, settings.SSL_PUBLIC_CRT)
            ikr_secure_service = internet.SSLServer(wss_port, site, contextFactory, interface=options['iface'])
            ikr_secure_service.setServiceParent(top_service)
        return top_service


service_maker = IKRServiceMaker()
