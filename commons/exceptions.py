# coding=utf-8


class ClientMessageValidationError(Exception):
    message = 'Ошибка валидации клиентского сообщения.'

    def __init__(self, message=None):
        super(ClientMessageValidationError, self).__init__(message or self.message)


class ServerEventMessageValidationError(Exception):
    message = 'Ошибка валидации серверного события.'

    def __init__(self, message=None):
        super(ServerEventMessageValidationError, self).__init__(message or self.message)


class ServerToSenderMessageValidationError(Exception):
    message = 'Ошибка валидации "сервер отправителю" сообщения.'

    def __init__(self, message=None):
        super(ServerToSenderMessageValidationError, self).__init__(message or self.message)


class ServerToRecipientMessageValidationError(Exception):
    message = 'Ошибка валидации "сервер получателю" сообщения.'

    def __init__(self, message=None):
        super(ServerToRecipientMessageValidationError, self).__init__(message or self.message)


class DuplicateRoomIdError(Exception):
    message = 'Комната с заданным id уже существует.'

    def __init__(self, message=None):
        super(DuplicateRoomIdError, self).__init__(message or self.message)


class RoomDoesNotExist(Exception):
    message = 'Комната с заданным id не существует.'

    def __init__(self, message=None):
        super(RoomDoesNotExist, self).__init__(message or self.message)
