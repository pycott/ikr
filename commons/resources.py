# coding=utf-8
from __future__ import absolute_import

from autobahn.twisted.resource import WebSocketResource
from six import PY3
from twisted.protocols.policies import ProtocolWrapper
from twisted.web.server import NOT_DONE_YET


class AuthWebSocketResource(WebSocketResource):
    def render(self, request):
        protocol = self._factory.buildProtocol(request.transport.getPeer(), self.username)
        if not protocol:
            request.setResponseCode(500)
            return b""
        transport, request.transport = request.transport, None

        if isinstance(transport, ProtocolWrapper):
            transport.wrappedProtocol = protocol
        else:
            transport.protocol = protocol
        protocol.makeConnection(transport)

        if PY3:

            data = request.method + b' ' + request.uri + b' HTTP/1.1\x0d\x0a'
            for h in request.requestHeaders.getAllRawHeaders():
                data += h[0] + b': ' + b",".join(h[1]) + b'\x0d\x0a'
            data += b"\x0d\x0a"
            data += request.content.read()

        else:
            data = "%s %s HTTP/1.1\x0d\x0a" % (request.method, request.uri)
            for h in request.requestHeaders.getAllRawHeaders():
                data += "%s: %s\x0d\x0a" % (h[0], ",".join(h[1]))
            data += "\x0d\x0a"
        protocol.dataReceived(data)

        return NOT_DONE_YET
