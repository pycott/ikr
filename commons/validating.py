import json

from schematics.exceptions import ModelValidationError
from schematics.models import Model
from schematics.transforms import blacklist
from schematics.types import StringType
from schematics.types.serializable import serializable

from commons.exceptions import ClientMessageValidationError, ServerEventMessageValidationError, \
    ServerToSenderMessageValidationError, ServerToRecipientMessageValidationError
from commons.utils import call_methods_startswith


class RbtSchematicsModel(Model):
    EXCEPTIONS_MAP = {
        'client': ClientMessageValidationError,
        'server_events': ServerEventMessageValidationError,
        'server_to_sender': ServerToSenderMessageValidationError,
        'server_to_recipient': ServerToRecipientMessageValidationError
    }

    type = StringType()

    def __init__(self, raw_data=None, deserialize_mapping=None, strict=False, **kwargs):
        rd = raw_data.copy() if raw_data else {}
        kw = kwargs.copy()
        rd.update(kw)
        super(RbtSchematicsModel, self).__init__(rd, deserialize_mapping=deserialize_mapping, strict=strict)
        call_methods_startswith(self, 'set')
        try:
            self.validate()
        except ModelValidationError as e:
            module_name = self.__module__.split('.')[-1]
            exception = self.EXCEPTIONS_MAP[module_name]
            raise exception(e.messages)

    @serializable
    def type(self):
        return self.get_type()

    @classmethod
    def get_type(cls):
        return cls._type

    def to_json(self, role=None):
        primitive = self.to_primitive(role=role) if role else self.to_primitive()
        return json.dumps(primitive)

    class Options:
        roles = {'history': blacklist('type')}
