# coding=utf-8
import inspect


def call_methods_startswith(obj, *args):
    for name, method in inspect.getmembers(obj, predicate=inspect.ismethod):
        for name_startswith in args:
            if name.startswith(name_startswith):
                method()
