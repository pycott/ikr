# coding=utf-8
import authkerb
from twisted.cred.portal import Portal
from twisted.web.guard import BasicCredentialFactory
from twisted.web.guard import HTTPAuthSessionWrapper
from twisted.web.resource import Resource

from auth.realm import IKRRealm
from commons.resources import AuthWebSocketResource
from server.factory import IKRServerFactory
from settings import main as settings


class IKRApi(Resource):
    def __init__(self):
        Resource.__init__(self)
        self.putChild('ws', self.get_ikr_ws())

    def get_ikr_ws(self):
        negotiateFactory = authkerb.NegotiateCredentialFactory(settings.KRB5_SERVICE.split('/', 1)[0])
        basicFactory = BasicCredentialFactory(settings.KRB5_REALM)

        portal = Portal(IKRRealm(AuthWebSocketResource, self.ikr_factory), settings.AUTH_CHECKERS)

        protected_resource = HTTPAuthSessionWrapper(portal, [negotiateFactory, basicFactory])
        return protected_resource

    @property
    def ikr_factory(self):
        if not hasattr(self, '_ikr_factory'):
            self._ikr_factory = IKRServerFactory()
        return self._ikr_factory
