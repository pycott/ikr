class StatusEnum(object):
    OFFLINE = 0
    AVAILABLE = 1
    AWAY = 2
    BUSY = 3
    INVISIBLE = 4
    IN_CALL = 5

    statuses = [OFFLINE, AVAILABLE, AWAY, BUSY, INVISIBLE, IN_CALL]
