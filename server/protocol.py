# coding=utf-8
import json
from datetime import datetime

from autobahn.twisted import WebSocketServerProtocol
from twisted.python.failure import Failure

from commons.exceptions import ClientMessageValidationError
from server import messages
from server.messages import server_events, server_to_sender, server_to_recipient, client


class IKRServerProtocol(WebSocketServerProtocol):
    STARTSWITH_PROCESSING_METHOD = '_processing_'
    TYPE = 'type'

    def __init__(self):
        super(IKRServerProtocol, self).__init__()
        self.username = None

    def onOpen(self):
        self.factory.on_connected(self)

    def onClose(self, wasClean, code, reason):
        self.factory.on_disconnected(self)

    def onMessage(self, payload, isBinary):
        try:
            dict_message = json.loads(payload)
            if self.TYPE not in dict_message:
                raise ClientMessageValidationError('Не указан тип сообщения.')
            method_name = '{}{}'.format(self.STARTSWITH_PROCESSING_METHOD, dict_message[self.TYPE])
            method = getattr(self, method_name)
            sender = self.username
            method(dict_message, sender)
        except Exception as e:
            self.on_error(Failure(e), payload)

    def on_error(self, failure, message):
        if failure.check(ClientMessageValidationError):
            error_message = messages.server_events.Error({
                'message': message,
                'exc_value': json.dumps(failure.value.args)
            })
            self.sendMessage(error_message.to_json())
        else:
            print(failure.getTraceback())

    def _processing_message(self, dict_message, sender):
        received_message = messages.client.Message(dict_message)
        recipient = received_message.username
        dt = datetime.utcnow()
        sender_message = messages.server_to_sender.ConfirmMessage(dict_message, datetime=dt)
        recipient_message = messages.server_to_recipient.Message(dict_message, username=sender, datetime=dt)
        self.factory.history_buffer.handle(received_message, sender_message, recipient_message)
        self.factory.send_to(sender_message.to_json(), sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_create_room(self, dict_message, sender):
        received_message = messages.client.CreateRoom(dict_message)
        room_name, recipients_usernames = received_message.room_name, received_message.usernames
        total_room_usernames = [sender]
        total_room_usernames.extend(recipients_usernames)
        room_id, room_name = self.factory.room_service.create_room(total_room_usernames, room_name)
        sender_message = messages.server_to_sender.CreateRoom(dict_message, room_id=room_id, room_name=room_name)
        recipient_message = messages.server_to_recipient.InviteToRoom(
                dict_message, room_id=room_id, usernames=total_room_usernames, room_name=room_name)
        self.factory.send_to(sender_message.to_json(), sender)
        self.factory.send_to(recipient_message.to_json(), *recipients_usernames)

    def _processing_invite_to_room(self, dict_message, sender):
        received_message = messages.client.InviteToRoom(dict_message)
        room = self.factory.room_service.get_room(received_message.room_id)
        invited_users = received_message.usernames
        notify_recipients = room.users
        recipient_notify_message = messages.server_to_recipient.InvitedIntoRoom(dict_message)

        room.add_users(invited_users)
        recipient_invite_message = messages.server_to_recipient.InviteToRoom(
                dict_message, usernames=room.users, room_name=room.name)

        self.factory.send_to(recipient_notify_message.to_json(), *notify_recipients)
        self.factory.send_to(recipient_invite_message.to_json(), *invited_users)

    def _processing_leave_room(self, dict_message, sender):
        received_message = messages.client.LeaveRoom(dict_message)
        room = self.factory.room_service.get_room(received_message.room_id)
        room.remove_users(sender)
        recipient_message = messages.server_to_recipient.LeaveRoom(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), *room.users)

    def _processing_message_room(self, dict_message, sender):
        received_message = messages.client.MessageRoom(dict_message)
        dt = datetime.utcnow()
        sender_message = messages.server_to_sender.ConfirmMessageRoom(dict_message, datetime=dt)
        recipient_message = messages.server_to_recipient.MessageRoom(dict_message, datetime=dt, username=sender)
        room = self.factory.room_service.get_room(received_message.room_id)
        room_users = room.users
        self.factory.history_buffer.handle(received_message, sender_message, recipient_message, room_users=room_users)
        room_users.remove(sender)
        self.factory.send_to(sender_message.to_json(), sender)
        self.factory.send_to(recipient_message.to_json(), *room_users)

    def _processing_call_offer(self, dict_message, sender):
        received_message = messages.client.CallOffer(dict_message)
        recipient = received_message.username
        recipient_message = messages.server_to_recipient.CallOffer(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_call_answer(self, dict_message, sender):
        received_message = messages.client.CallAnswer(dict_message)
        recipient = received_message.username
        recipient_message = messages.server_to_recipient.CallAnswer(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_call_ice(self, dict_message, sender):
        received_message = messages.client.CallIce(dict_message)
        recipient = received_message.username
        recipient_message = messages.server_to_recipient.CallIce(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_call(self, dict_message, sender):
        received_message = messages.client.Call(dict_message)
        recipient = received_message.username
        recipient_message = messages.server_to_recipient.Call(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_take_call(self, dict_message, sender):
        received_message = messages.client.TakeCall(dict_message)
        recipient = received_message.username
        recipient_message = messages.server_to_recipient.TakeCall(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_cancel_call(self, dict_message, sender):
        received_message = messages.client.CancelCall(dict_message)
        recipient = received_message.username
        recipient_message = messages.server_to_recipient.CancelCall(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_hangup(self, dict_message, sender):
        received_message = messages.client.Hangup(dict_message)
        recipient = received_message.username
        recipient_message = messages.server_to_recipient.Hangup(dict_message, username=sender)
        self.factory.send_to(recipient_message.to_json(), recipient)

    def _processing_status(self, dict_message, sender):
        received_message = messages.client.Status(dict_message)
        self.factory.change_status(sender, received_message.status)

    def _processing_history(self, dict_message, sender):
        received_message = messages.client.History(dict_message)
        history = self.factory.history_buffer.get(sender, received_message.limit, received_message.offset)
        sender_message = messages.server_to_sender.History(history=history)
        self.sendMessage(sender_message.to_json())

    def _processing_call_room(self, dict_message, sender):
        received_message = messages.client.CallRoom(dict_message)
        recipient_message = messages.server_to_recipient.CallRoom(dict_message, username=sender)

        room = self.factory.room_service.get_room(received_message.room_id)
        recipients = room.create_call(sender)

        self.factory.send_to(recipient_message.to_json(), *recipients)

    def _processing_take_call_room(self, dict_message, sender):
        received_message = messages.client.TakeCallRoom(dict_message)
        recipient_message = messages.server_to_recipient.TakeCallRoom(dict_message, username=sender)

        room = self.factory.room_service.get_room(received_message.room_id)
        recipients = room.get_call_acceptors()

        room.accept_call(sender)
        self.factory.send_to(recipient_message.to_json(), *recipients)

    def _processing_cancel_call_room(self, dict_message, sender):
        received_message = messages.client.CancelCallRoom(dict_message)
        recipient_message = messages.server_to_recipient.CancelCallRoom(dict_message, username=sender)

        room = self.factory.room_service.get_room(received_message.room_id)
        room.cancel_call(sender)

        recipients = room.get_call_acceptors()
        self.factory.send_to(recipient_message.to_json(), *recipients)

    def _processing_hangup_room(self, dict_message, sender):
        received_message = messages.client.HangupRoom(dict_message)
        recipient_message = messages.server_to_recipient.HangupRoom(dict_message, username=sender)

        room = self.factory.room_service.get_room(received_message.room_id)
        room.hangup_call(sender)

        recipients = room.get_call_acceptors()
        self.factory.send_to(recipient_message.to_json(), *recipients)
