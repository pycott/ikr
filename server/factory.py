# coding=utf-8

from autobahn.twisted import WebSocketServerFactory

from server import messages, StatusEnum
from server.history import MemoryBuffer
from server.messages import server_events
from server.protocol import IKRServerProtocol
from server.services import IKRRoomService
from settings import main as settings


class IKRServerFactory(WebSocketServerFactory):
    protocol = IKRServerProtocol

    def __init__(self, *args, **kwargs):
        kwargs['debug'] = settings.DEBUG
        kwargs['debugCodePaths'] = settings.DEBUG
        super(IKRServerFactory, self).__init__(*args, **kwargs)
        self.users = {}
        self.online_list = {}
        self.room_service = IKRRoomService()
        self.history_buffer = MemoryBuffer()

    def buildProtocol(self, addr, username):
        protocol = super(IKRServerFactory, self).buildProtocol(addr)
        protocol.username = username
        protocol.status = self._get_last_status(username)
        return protocol

    def on_connected(self, con):
        if con.username not in self.users:
            self._send_new_status(con.username, con.status)
        self._add_user(con)
        self._send_online_list(con)

    def on_disconnected(self, con):
        self._remove_user(con)
        if con.username not in self.users:
            self._send_new_status(con.username, StatusEnum.OFFLINE)

    def change_status(self, username, status):
        connections = self.users.get(username, [])
        for con in connections:
            con.status = status
        self.online_list[username] = status
        self._send_new_status(username, status)

    def send_to(self, json_message, *args):
        for user in args:
            connections = self.users.get(user, [])
            for con in connections:
                con.sendMessage(json_message)

    def _add_user(self, con):
        username = con.username
        connections = self.users.setdefault(username, [])
        if not connections:
            self.online_list[username] = con.status
        connections.append(con)

    def _remove_user(self, con):
        username = con.username
        connections = self.users.get(username, [])
        connections.remove(con)
        if not connections:
            self.online_list.pop(username, None)
            self.users.pop(username)

    def _get_last_status(self, username):
        return StatusEnum.AVAILABLE

    def _send_all(self, json_message):
        for connections in self.users.values():
            for con in connections:
                con.sendMessage(json_message)

    def _send_online_list(self, con):
        con.sendMessage(messages.server_events.OnlineList(users=self.online_list).to_json())

    def _send_new_status(self, username, status):
        self._send_all(messages.server_events.Status(username=username, status=status).to_json())
