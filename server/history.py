# coding=utf-8


class MemoryBuffer(object):
    STARTSWITH_PROCESSING_METHOD = '_processing_'
    TYPE = 'type'
    HISTORY_ROLE = 'history'

    def __init__(self):
        self.db = {}

    def handle(self, *args, **kwargs):
        types = {getattr(message, self.TYPE) for message in args}
        for _type in types:
            method_name = '{}{}'.format(self.STARTSWITH_PROCESSING_METHOD, _type)
            method = getattr(self, method_name, None)
            if callable(method):
                method(*args, **kwargs)

    def get(self, username, limit=None, offset=0):
        full_history = self.db.get(username)
        if not full_history:
            return {}
        cropped_history = {}
        if limit:
            for history_with, messages in full_history.iteritems():
                cropped_history[history_with] = messages[offset:offset + limit]
            return cropped_history
        elif offset:
            for history_with, messages in full_history.iteritems():
                cropped_history[history_with] = messages[offset:]
            return cropped_history
        return full_history

    def _save(self, user, history_with, message):
        user = self.db.setdefault(user, {})
        history_with = user.setdefault(history_with, [])
        history_with.append(message)

    def _processing_message(self, received_message, sender_message, recipient_message, **kwargs):
        message = recipient_message.to_primitive(role=self.HISTORY_ROLE)
        self._save(recipient_message.username, sender_message.username, message)
        self._save(sender_message.username, recipient_message.username, message)

    def _processing_message_room(self, received_message, sender_message, recipient_message, **kwargs):
        message = recipient_message.to_primitive(role=self.HISTORY_ROLE)
        for user in kwargs['room_users']:
            self._save(user, recipient_message.room_id, message)
