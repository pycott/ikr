# coding=utf-8
import copy


class Room(object):
    def __init__(self, id, name, usernames):
        self.id = id
        self.name = name
        self._users = usernames

    @property
    def users(self):
        return copy.copy(self._users)

    def add_users(self, args):
        self._users.extend(args)

    def remove_users(self, args):
        for username in args:
            if username in self._users:
                self._users.remove(username)

    def create_call(self, initiator):
        self.awaiting_confirmation_call = filter(lambda username: username != initiator, self.users)
        self.call_acceptors = {initiator}
        return self.awaiting_confirmation_call

    def accept_call(self, accept_user):
        if accept_user in self.awaiting_confirmation_call:
            self.awaiting_confirmation_call.remove(accept_user)
            self.call_acceptors.add(accept_user)

    def cancel_call(self, cancel_user):
        if cancel_user in self.awaiting_confirmation_call:
            self.awaiting_confirmation_call.remove(cancel_user)

    def hangup_call(self, hangup_user):
        if hangup_user in self.call_acceptors:
            self.call_acceptors.remove(hangup_user)

    def get_call_acceptors(self):
        # todo refactoring
        return copy.copy(self.call_acceptors)

    # todo test filter vs list.remove
    def users_without(self, username):
        return filter(lambda user: user != username, self.users)
