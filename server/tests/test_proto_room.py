# coding=utf-8
import json

from twisted.internet.defer import DeferredList

from server.tests import IKRServerProtocolTestCase


class IKRServerProtocolRoomTestCase(IKRServerProtocolTestCase):
    def test_create_room_withou_name(self):
        self.addCleanup(self.cleaning)
        creator, recipient1, recipient2 = users = 'user10', 'user1', 'user2'
        recipients = [recipient1, recipient2]

        clients = self.get_clients(*users)

        room_id, d = self.create_room(clients, creator, recipients)
        return d.addCallback(self.check_count_connected_users, count=3)

    def test_create_room(self):
        self.addCleanup(self.cleaning)
        creator, recipient1, recipient2 = users = 'user10', 'user1', 'user2'
        recipients = [recipient1, recipient2]
        room_name = 'myqwerty'

        clients = self.get_clients(*users)

        room_id, d = self.create_room(clients, creator, recipients, room_name)
        return d.addCallback(self.check_count_connected_users, count=3)

    # todo test for RoomDoesNotExist
    def test_invite_to_room(self):
        self.addCleanup(self.cleaning)
        users = ['user10', 'user1', 'user2', 'user3', 'user4']
        invite_sender, notify_recipient1, notify_recipient2, invite_recipient1, invite_recipient2 = users

        create_room_recipients = [notify_recipient1, notify_recipient2]
        notify_recipients = [invite_sender, notify_recipient1, notify_recipient2]
        invite_recipients = [invite_recipient1, invite_recipient2]

        notify_clients = self.get_clients(*notify_recipients)
        invite_clients = self.get_clients(*invite_recipients)

        room_name = 'bochok_webinar'
        room_id, d = self.create_room(notify_clients, invite_sender, create_room_recipients, room_name)

        client_message = {'type': 'invite_to_room', 'usernames': invite_recipients, 'room_id': room_id}
        invite_message = {'type': 'invite_to_room', 'room_id': room_id, 'usernames': users, 'room_name': room_name}
        notify_message = {'type': 'invited_into_room', 'room_id': room_id, 'usernames': invite_recipients}

        def invite_to_room(res):
            self.check_count_connected_users(count=5)
            notify_clients[invite_sender].client.sendMessage(json.dumps(client_message))
            d_list = [
                notify_clients[notify_recipient1].on('invited_into_room', got_message, callbackArgs=[notify_message]),
                notify_clients[notify_recipient2].on('invited_into_room', got_message, callbackArgs=[notify_message]),
                invite_clients[invite_recipient1].on('invite_to_room', got_message, callbackArgs=[invite_message]),
                invite_clients[invite_recipient2].on('invite_to_room', got_message, callbackArgs=[invite_message]),
            ]
            return DeferredList(d_list, consumeErrors=True).addCallback(self.cb_check_dlist)

        def got_message(message, correct_message):
            self.check_count_connected_users(count=5)
            self.assertDictEqual(message, correct_message)

        return d.addCallback(invite_to_room)

    def test_leave_room(self):
        self.addCleanup(self.cleaning)
        leave_sender, recipient1, recipient2 = users = 'user10', 'user1', 'user2'
        recipients = [recipient1, recipient2]

        clients = self.get_clients(*users)
        room_id, d = self.create_room(clients, leave_sender, recipients)

        client_message = {'type': 'leave_room', 'room_id': room_id,}
        recipient_message = {'type': 'leave_room', 'room_id': room_id, 'username': leave_sender}

        def leave(res):
            self.check_count_connected_users(count=3)
            clients[leave_sender].client.sendMessage(json.dumps(client_message))
            d_list = [
                clients[recipient1].on('leave_room', got_notify),
                clients[recipient2].on('leave_room', got_notify),
            ]
            return DeferredList(d_list, consumeErrors=True).addCallback(self.cb_check_dlist)

        def got_notify(message):
            self.check_count_connected_users(count=3)
            self.assertEqual(message, recipient_message)

        return d.addCallback(leave)

    def test_message_room(self):
        self.addCleanup(self.cleaning)
        sender, recipient1, recipient2 = users = 'user10', 'user1', 'user2'
        recipients = [recipient1, recipient2]

        clients = self.get_clients(*users)
        room_id, d = self.create_room(clients, sender, recipients)

        d_list = [
            clients[sender].on('onOpen'),
            clients[recipient1].on('onOpen'),
            clients[recipient2].on('onOpen'),
        ]

        d = DeferredList(d_list, consumeErrors=True). \
            addCallback(self.cb_check_dlist). \
            addCallback(self.message_room, clients, sender, room_id, 'hello')
        return d

    def test_call_room(self):
        self.addCleanup(self.cleaning)
        sender, user2, user3, user4, user5 = users = 'user1', 'user2', 'user3', 'user4', 'user5'

        clients = self.get_clients(*users)
        room_id, d = self.create_room(clients, sender, [user2, user3, user4, user5])

        client_call_room_message = {'type': 'call_room', 'room_id': room_id}
        recipient_call_room_message = {'type': 'call_room', 'room_id': room_id, 'username': sender}

        client_take_call_room_message = {'type': 'take_call_room', 'room_id': room_id}
        client_cancel_call_room_message = {'type': 'cancel_call_room', 'room_id': room_id}
        client_hangup_room_message = {'type': 'hangup_room', 'room_id': room_id}

        def user_cancel_call(res, user, recipients):
            self.check_count_connected_users(count=5)
            correct_message = {'type': 'cancel_call_room', 'room_id': room_id, 'username': user}
            clients[user].client.sendMessage(json.dumps(client_cancel_call_room_message))
            d_list = [
                clients[username].on('cancel_call_room', lambda msg: self.assertEqual(msg, correct_message))
                for username in recipients
                ]
            return DeferredList(d_list, consumeErrors=True).addCallback(self.cb_check_dlist)

        def user_take_call(res, user, recipients):
            self.check_count_connected_users(count=5)
            correct_message = {'type': 'take_call_room', 'room_id': room_id, 'username': user}
            clients[user].client.sendMessage(json.dumps(client_take_call_room_message))
            d_list = [
                clients[username].on('take_call_room', lambda msg: self.assertEqual(msg, correct_message))
                for username in recipients
                ]
            return DeferredList(d_list, consumeErrors=True).addCallback(self.cb_check_dlist)

        def user_hangup(res, user, recipients):
            self.check_count_connected_users(count=5)
            correct_message = {'type': 'hangup_room', 'room_id': room_id, 'username': user}
            clients[user].client.sendMessage(json.dumps(client_hangup_room_message))
            d_list = [
                clients[username].on('hangup_room', lambda msg: self.assertEqual(msg, correct_message))
                for username in recipients
                ]
            return DeferredList(d_list, consumeErrors=True).addCallback(self.cb_check_dlist)

        def send_call_room(res):
            self.check_count_connected_users(count=5)
            clients[sender].client.sendMessage(json.dumps(client_call_room_message))
            d_list = [
                clients[user2].on('call_room', lambda msg: self.assertEqual(msg, recipient_call_room_message)),
                clients[user3].on('call_room', lambda msg: self.assertEqual(msg, recipient_call_room_message)),
                clients[user4].on('call_room', lambda msg: self.assertEqual(msg, recipient_call_room_message)),
                clients[user5].on('call_room', lambda msg: self.assertEqual(msg, recipient_call_room_message)),
            ]
            return DeferredList(d_list, consumeErrors=True). \
                addCallback(self.cb_check_dlist). \
                addCallback(user_take_call, user2, [sender]). \
                addCallback(user_take_call, user3, [sender, user2]). \
                addCallback(user_cancel_call, user4, [sender, user2, user3]). \
                addCallback(user_take_call, user5, [sender, user2, user3]). \
                addCallback(user_hangup, sender, [user2, user3, user5]). \
                addCallback(user_hangup, user3, [user2, user5])

        return d.addCallback(send_call_room)
