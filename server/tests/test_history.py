# coding=utf-8
from __future__ import unicode_literals

import json

from twisted.internet.defer import DeferredList

from server.tests import IKRServerProtocolTestCase


class MemoryBufferTestCase(IKRServerProtocolTestCase):
    maxDiff = None

    def send_private_messages(self, client_message, messages, correct_history_user1, correct_history_user2):
        user1, user2 = users = 'user1', 'user2'
        clients = self.get_clients(*users)

        d_list = [
            clients[user1].on('onOpen'),
            clients[user2].on('onOpen'),
        ]

        def send_message(res, user, correct_history):
            clients[user].client.sendMessage(json.dumps(client_message))
            return clients[user].on('history', check_history, callbackArgs=[correct_history])

        def check_history(message, correct_history):
            history = message['history']
            self.assertNotEqual(history, {})
            for history_with, messages in history.iteritems():
                self.assertIn(history_with, ['user1', 'user2'])
                for message in messages:
                    message.pop('datetime', None)
                self.assertEqual(history[history_with], correct_history[history_with])

        d = DeferredList(d_list, consumeErrors=True). \
            addCallback(self.cb_check_dlist). \
            addCallback(self.private_message, clients, user1, user2, messages[0]). \
            addCallback(self.private_message, clients, user1, user2, messages[1]). \
            addCallback(self.private_message, clients, user2, user1, messages[2]). \
            addCallback(self.private_message, clients, user2, user1, messages[3]). \
            addCallback(send_message, user1, correct_history_user1). \
            addCallback(send_message, user2, correct_history_user2)
        return d

    def test_private_message(self):
        self.addCleanup(self.cleaning)
        user1, user2 = 'user1', 'user2'
        messages = ["hello, vasya", "how are u?", "dobrechkoe ytrechko", "i'm fine!"]

        client_message = {
            'type': 'history'
        }

        correct_history_user1 = {
            user2: [
                {'message': messages[0], 'username': user1},
                {'message': messages[1], 'username': user1},
                {'message': messages[2], 'username': user2},
                {'message': messages[3], 'username': user2},
            ]
        }
        correct_history_user2 = {
            user1: [
                {'message': messages[0], 'username': user1},
                {'message': messages[1], 'username': user1},
                {'message': messages[2], 'username': user2},
                {'message': messages[3], 'username': user2},
            ]
        }

        return self.send_private_messages(client_message, messages, correct_history_user1, correct_history_user2)

    def test_private_message_limit(self):
        self.addCleanup(self.cleaning)
        user1, user2 = 'user1', 'user2'
        messages = ["hello, vasya", "how are u?", "dobrechkoe ytrechko", "i'm fine!"]

        client_message = {
            'type': 'history',
            'limit': 1
        }

        correct_history_user1 = {
            user2: [
                {'message': messages[0], 'username': user1},
            ]
        }
        correct_history_user2 = {
            user1: [
                {'message': messages[0], 'username': user1},
            ]
        }

        return self.send_private_messages(client_message, messages, correct_history_user1, correct_history_user2)

    def test_private_offset(self):
        self.addCleanup(self.cleaning)
        user1, user2 = 'user1', 'user2'
        messages = ["hello, vasya", "how are u?", "dobrechkoe ytrechko", "i'm fine!"]

        client_message = {
            'type': 'history',
            'offset': 1
        }

        correct_history_user1 = {
            user2: [
                {'message': messages[1], 'username': user1},
                {'message': messages[2], 'username': user2},
                {'message': messages[3], 'username': user2},
            ]
        }
        correct_history_user2 = {
            user1: [
                {'message': messages[1], 'username': user1},
                {'message': messages[2], 'username': user2},
                {'message': messages[3], 'username': user2},
            ]
        }

        return self.send_private_messages(client_message, messages, correct_history_user1, correct_history_user2)

    def test_private_message_limit_offset(self):
        self.addCleanup(self.cleaning)
        user1, user2 = 'user1', 'user2'
        messages = ["hello, vasya", "how are u?", "dobrechkoe ytrechko", "i'm fine!"]

        client_message = {
            'type': 'history',
            'limit': 2,
            'offset': 2
        }

        correct_history_user1 = {
            user2: [
                {'message': messages[2], 'username': user2},
                {'message': messages[3], 'username': user2},
            ]
        }
        correct_history_user2 = {
            user1: [
                {'message': messages[2], 'username': user2},
                {'message': messages[3], 'username': user2},
            ]
        }

        return self.send_private_messages(client_message, messages, correct_history_user1, correct_history_user2)

    def test_message_room(self):
        self.addCleanup(self.cleaning)
        user1, user2, user3 = users = 'user1', 'user2', 'user3'
        messages = ["hello, vasya", "how are u?", "dobrechkoe ytrechko", "i'm fine!"]

        client_message = {'type': 'history'}

        clients = self.get_clients(*users)
        room_id, d = self.create_room(clients, user1, [user2, user3])

        correct_history = {
            str(room_id): [
                {'message': messages[0], 'username': user1},
                {'message': messages[1], 'username': user2},
                {'message': messages[2], 'username': user3},
                {'message': messages[3], 'username': user1},
            ]
        }

        def send_message(res, user, correct_history):
            clients[user].client.sendMessage(json.dumps(client_message))
            return clients[user].on('history', check_history, callbackArgs=[correct_history])

        def check_history(message, correct_history):
            history = message['history']
            self.assertNotEqual(history, {})
            for history_with, messages in history.iteritems():
                self.assertEqual(history_with, str(room_id))
                for message in messages:
                    message.pop('datetime', None)
                self.assertEqual(history[history_with], correct_history[history_with])

        d_list = [clients[user1].on('onOpen'), clients[user2].on('onOpen'), clients[user3].on('onOpen')]
        d = DeferredList(d_list, consumeErrors=True). \
            addCallback(self.cb_check_dlist). \
            addCallback(self.message_room, clients, user1, room_id, messages[0]). \
            addCallback(self.message_room, clients, user2, room_id, messages[1]). \
            addCallback(self.message_room, clients, user3, room_id, messages[2]). \
            addCallback(self.message_room, clients, user1, room_id, messages[3]). \
            addCallback(send_message, user1, correct_history). \
            addCallback(send_message, user2, correct_history). \
            addCallback(send_message, user3, correct_history)

        return d
