# coding=utf-8
import json

from twisted.internet.defer import DeferredList

from server.tests import IKRServerProtocolTestCase


class IKRServerProtocolPrivateTestCase(IKRServerProtocolTestCase):
    def test_private_message(self):
        self.addCleanup(self.cleaning)
        user1, user2 = users = 'user1', 'user2'
        clients = self.get_clients(*users)
        d_list = [
            clients[user1].on('onOpen'),
            clients[user2].on('onOpen'),
        ]
        d = DeferredList(d_list, consumeErrors=True). \
            addCallback(self.cb_check_dlist). \
            addCallback(self.private_message, clients, user1, user2, 'world')
        return d

    def _send_simple_msg(self, sender, recipient, message_type, client_message, recipient_message):
        self.addCleanup(self.cleaning)
        clients = self.get_clients(sender, recipient)

        def send_msg(res):
            self.check_count_connected_users(count=2)
            clients[sender].client.sendMessage(json.dumps(client_message))
            return clients[recipient].on(message_type, recipient_got_message)

        def recipient_got_message(message):
            self.check_count_connected_users(count=2)
            self.assertDictEqual(message, recipient_message)

        return clients[sender].on('onOpen', send_msg)

    def test_call_offer(self):
        sender = 'user10'
        recipient = 'user2'
        message_type = 'call_offer'

        client_message = {'type': message_type, 'offer': '$sdp_offer1$', 'username': recipient}
        recipient_message = {'type': message_type, 'offer': '$sdp_offer1$', 'username': sender}

        return self._send_simple_msg(sender, recipient, message_type, client_message, recipient_message)

    def test_call_answer(self):
        sender = 'user10'
        recipient = 'user2'
        message_type = 'call_answer'

        client_message = {'type': message_type, 'answer': '$sdp_answer1$', 'username': recipient}
        recipient_message = {'type': message_type, 'answer': '$sdp_answer1$', 'username': sender}

        return self._send_simple_msg(sender, recipient, message_type, client_message, recipient_message)

    def test_call_ice(self):
        sender = 'user10'
        recipient = 'user2'
        message_type = 'call_ice'

        client_message = {'type': message_type, 'ice': '$ice_candidates1$', 'username': recipient}
        recipient_message = {'type': message_type, 'ice': '$ice_candidates1$', 'username': sender}

        return self._send_simple_msg(sender, recipient, message_type, client_message, recipient_message)

    def test_call(self):
        sender = 'user10'
        recipient = 'user2'
        message_type = 'call'

        client_message = {'type': message_type, 'username': recipient}
        recipient_message = {'type': message_type, 'username': sender}

        return self._send_simple_msg(sender, recipient, message_type, client_message, recipient_message)

    def test_cancel_call(self):
        sender = 'user10'
        recipient = 'user2'
        message_type = 'cancel_call'

        client_message = {'type': message_type, 'username': recipient}
        recipient_message = {'type': message_type, 'username': sender}

        return self._send_simple_msg(sender, recipient, message_type, client_message, recipient_message)

    def test_take_call(self):
        sender = 'user10'
        recipient = 'user2'
        message_type = 'take_call'

        client_message = {'type': message_type, 'username': recipient}
        recipient_message = {'type': message_type, 'username': sender}

        return self._send_simple_msg(sender, recipient, message_type, client_message, recipient_message)

    def test_hangup(self):
        sender = 'user10'
        recipient = 'user2'
        message_type = 'hangup'

        client_message = {'type': message_type, 'username': recipient}
        recipient_message = {'type': message_type, 'username': sender}

        return self._send_simple_msg(sender, recipient, message_type, client_message, recipient_message)
