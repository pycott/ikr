# coding=utf-8
import json

from server import StatusEnum
from server.tests import IKRServerProtocolTestCase


class IKRServerProtocolBaseTestCase(IKRServerProtocolTestCase):
    def test_online_empty(self):
        self.addCleanup(self.cleaning)
        user1 = 'user1'
        clients = self.get_clients(user1)

        correct_message = {'type': 'online_list', 'users': {'user1': StatusEnum.AVAILABLE}}

        def got_online_msg(message):
            users = sorted(self.server_factory.users.keys())
            self.check_count_connected_users(count=1)
            self.assertEqual(users, [user1])
            self.assertEquals(message, correct_message)

        return clients[user1].on('online_list', got_online_msg)

    def test_online_users(self):
        self.addCleanup(self.cleaning)

        self.get_client('user1')
        self.get_client('user2')
        user3 = self.get_client('user3')

        correct_message = {'type': 'online_list', 'users': {'user1': StatusEnum.AVAILABLE,
                                                            'user2': StatusEnum.AVAILABLE,
                                                            'user3': StatusEnum.AVAILABLE}}

        def got_online_msg(message):
            users = sorted(self.server_factory.users.keys())
            self.check_count_connected_users(count=3)
            self.assertEquals(users, ['user1', 'user2', 'user3'])
            self.assertEquals(message, correct_message)

        return user3.on('online_list', got_online_msg)

    def test_join_user(self):
        self.addCleanup(self.cleaning)

        user1 = self.get_client('user1')
        self.get_client('user2')

        correct_message = {'type': 'status', 'username': 'user2', 'status': StatusEnum.AVAILABLE}

        def got_message(message):
            self.check_count_connected_users(count=2)
            self.assertEquals(message, correct_message)

        return user1.on('status', got_message)

    def test_leave_user(self):
        self.addCleanup(self.cleaning)

        receiver_factory = self.get_client('user1')
        leaver_factory = self.get_client('user2')

        correct_message = {'type': 'status', 'username': 'user2', 'status': StatusEnum.OFFLINE}

        def disconnect(res):
            self.check_count_connected_users(count=2)
            self.discon_client(leaver_factory.reactor_con)
            return receiver_factory.on('status', got_leave_msg)

        def got_leave_msg(message):
            self.assertEquals(message, correct_message)
            self.check_count_connected_users(count=1)

        return leaver_factory.on('onOpen', disconnect)

    def test_wrong_message(self):
        self.addCleanup(self.cleaning)
        sender = 'user1'

        client = self.get_client(sender)

        client_message = {'type': 'message', 'username': 'adf', 'message': 'mymsg'}
        error_message = {'type': 'error', 'message': json.dumps(client_message)}

        def send_msg(res):
            self.check_count_connected_users(count=1)
            client.client.sendMessage(json.dumps(client_message))
            return client.on('error', got_message)

        def got_message(message):
            self.check_count_connected_users(count=1)
            self.assertIn('exc_value', message)
            validation_errors = json.loads(message['exc_value'])
            for error in validation_errors:
                self.assertIn('uuid', error)
            del message['exc_value']
            self.assertDictEqual(message, error_message)

        return client.on('onOpen', send_msg)

    def test_change_status(self):
        self.addCleanup(self.cleaning)
        sender, recipient = users = 'user1', 'user2'
        new_status = StatusEnum.BUSY

        clients = self.get_clients(*users)

        client_message = {'type': 'status', 'status': new_status}
        recipient_message = {'type': 'status', 'status': new_status, 'username': sender}
        online_list = {sender: new_status, recipient: StatusEnum.AVAILABLE}

        def change_status(res):
            clients[sender].client.sendMessage(json.dumps(client_message))
            return clients[recipient].on('status', got_message)

        def got_message(message):
            self.assertDictEqual(message, recipient_message)
            self.assertDictEqual(self.server_factory.online_list, online_list)
            self.assertEquals(self.server_factory.users[sender][0].status, new_status)

        return clients[sender].on('onOpen', change_status)

    def test_multiple_con_online_list(self):
        self.addCleanup(self.cleaning)

        self.get_client('user1')
        self.get_client('user1')
        user2 = self.get_client('user2')

        correct_online_list = {'type': 'online_list', 'users': {'user1': StatusEnum.AVAILABLE, 'user2': StatusEnum.AVAILABLE}}

        def got_online(msg):
            self.check_count_connected_users(count=2)
            self.assertEqual(self.server_factory.getConnectionCount(), 3)
            self.assertDictEqual(msg, correct_online_list)

        return user2.on('online_list', got_online)

    def test_multiple_con_join_user(self):
        self.addCleanup(self.cleaning)

        user1 = self.get_client('user1')
        self.get_client('user2')
        self.get_client('user2')

        correct_message = {'type': 'status', 'username': 'user2', 'status': StatusEnum.AVAILABLE}

        def got_message(message):
            self.check_count_connected_users(count=2)
            self.assertEqual(self.server_factory.getConnectionCount(), 3)
            self.assertEquals(message, correct_message)

        return user1.on('status', got_message)

    def test_multiple_con_leave_user2(self):
        self.addCleanup(self.cleaning)

        receiver = self.get_client('user1')
        leaver1_1 = self.get_client('user2')
        leaver1_2 = self.get_client('user2')

        correct_message = {'type': 'status', 'username': 'user2', 'status': StatusEnum.OFFLINE}

        def disconnect1(res):
            self.check_count_connected_users(count=2)
            self.assertEqual(self.server_factory.getConnectionCount(), 3)
            self.discon_client(leaver1_1.reactor_con)
            return leaver1_1.on('onClose', disconnect2)

        def disconnect2(res):
            self.discon_client(leaver1_2.reactor_con)
            return receiver.on('status', got_message)

        def got_message(message):
            self.assertEquals(message, correct_message)
            self.check_count_connected_users(count=1)
            self.assertEqual(self.server_factory.getConnectionCount(), 1)

        return leaver1_1.on('onOpen', disconnect1)
