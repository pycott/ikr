# coding=utf-8
import base64
import copy
import json

from autobahn.twisted import WebSocketClientProtocol, WebSocketClientFactory
from twisted.internet import reactor
from twisted.internet.defer import Deferred, passthru
from twisted.internet.defer import DeferredList
from twisted.trial._asynctest import TestCase
from twisted.web.server import Site

from server.api import IKRApi
from server.services import IKRRoomService
from settings.develop import DEBUG_USER_PASSWORD

EVENT_STARTSWITH = '_event_'


class IKRServerProtocolTestCase(TestCase):
    interface = '127.0.0.1'
    timeout = 1

    def setUp(self):
        self.api = IKRApi()
        self.server_factory = self.api.ikr_factory
        self.connections = []
        self.room_service = IKRRoomService()
        self.server_reactor = reactor.listenTCP(0, Site(self.api), interface=self.interface)
        self.address = self.server_reactor.getHost()
        self.ws_url = 'ws://{}:{}/ws/'.format(self.address.host, self.address.port)

    def tearDown(self):
        self.cleaning()
        self.cleaning()

    def cleaning(self, *args, **kwargs):
        for con in self.connections:
            self.discon_client(con)
        if hasattr(self, 'server_reactor'):
            return self.server_reactor.stopListening()

    def discon_client(self, con):
        if con in self.connections:
            self.connections.remove(con)
        if con.state == 'connected':
            con.disconnect()

    def get_client(self, username):
        basic_header = base64.b64encode('{}:{}'.format(username, DEBUG_USER_PASSWORD))
        headers = {'Authorization': 'Basic {}'.format(basic_header)}
        factory = IKRTestFactory(self.ws_url, headers=headers)
        con = reactor.connectTCP(self.address.host, self.address.port, factory)
        con.username = username
        factory.reactor_con = con
        self.connections.append(factory.reactor_con)
        return factory

    def get_clients(self, *args, **kwargs):
        kw = copy.copy(kwargs)
        for username in args:
            kw[username] = username
        clients = {}
        for prefix, username in kw.iteritems():
            factory = self.get_client(username)
            clients[prefix] = factory
        return clients

    def check_count_connected_users(self, res=None, count=None):
        self.assertEqual(len(self.server_factory.online_list.keys()), count)
        self.assertEqual(len(self.server_factory.users.keys()), count)

    def cb_check_dlist(self, d_results):
        for d_result in d_results:
            complited, res = d_result
            if not complited:
                return res

    def create_room(self, clients, creator, receivers, room_name=None):
        total_room_usernames = [creator]
        total_room_usernames.extend(receivers)

        client_message = {'type': 'create_room', 'uuid': 'abv11', 'usernames': receivers,}
        if room_name: client_message['room_name'] = room_name

        correct_room_id, room_name = self.room_service.create_room(total_room_usernames, room_name)

        sender_message = {'type': 'create_room', 'uuid': 'abv11', 'usernames': receivers, 'room_id': correct_room_id}
        if room_name: sender_message['room_name'] = room_name

        recipient_message = {'type': 'invite_to_room', 'usernames': total_room_usernames, 'room_id': correct_room_id}
        if room_name: recipient_message['room_name'] = room_name

        def create_room(res):
            clients[creator].client.sendMessage(json.dumps(client_message))
            d_list = []
            for username, factory in clients.iteritems():
                if username == creator:
                    d_list.append(factory.on('create_room', got_message, callbackArgs=[sender_message]))
                else:
                    d_list.append(factory.on('invite_to_room', got_message, callbackArgs=[recipient_message]))
            return DeferredList(d_list, consumeErrors=True).addCallback(self.cb_check_dlist)

        def got_message(message, correct_message):
            self.assertEqual(len(self.server_factory.room_service._rooms), 1)
            self.assertEqual(message, correct_message)

        return correct_room_id, clients[creator].on('onOpen', create_room)

    def private_message(self, res, clients, sender, recipient, message):
        client_message = {'type': 'message', 'uuid': 'abv11', 'username': recipient, 'message': message}
        sender_message = {'type': 'confirm_message', 'uuid': 'abv11', 'username': recipient}
        recipient_message = {'type': 'message', 'username': sender, 'message': message}

        def send_message():
            clients[sender].client.sendMessage(json.dumps(client_message))
            d_list = [
                clients[sender].on('confirm_message', got_message, callbackArgs=[sender_message]),
                clients[recipient].on('message', got_message, callbackArgs=[recipient_message]),
            ]
            return DeferredList(d_list, consumeErrors=True)

        def got_message(message, correct_message):
            self.assertIsNotNone(message)
            self.assertIsNotNone(message.get('datetime', None))
            del message['datetime']
            self.assertDictEqual(message, correct_message)

        return send_message()

    def message_room(self, res, clients, sender, room_id, message):
        client_message = {'type': 'message_room', 'uuid': 'abv11', 'room_id': room_id, 'message': message}
        sender_message = {'type': 'confirm_message_room', 'uuid': 'abv11', 'room_id': room_id}
        recipient_message = {'type': 'message_room', 'username': sender, 'message': message}

        def send_message():
            clients[sender].client.sendMessage(json.dumps(client_message))
            d_list = []

            for username, factory in clients.iteritems():
                if username == sender:
                    d_list.append(factory.on('confirm_message_room', got_message, callbackArgs=[sender_message]))
                else:
                    d_list.append(factory.on('message_room', got_message, callbackArgs=[recipient_message]))

            return DeferredList(d_list, consumeErrors=True)

        def got_message(message, correct_message):
            self.assertIsNotNone(message)
            self.assertIsNotNone(message.get('datetime', None))
            del message['datetime']
            self.assertDictEqual(message, correct_message)

        return send_message()


class IKRTestProtocol(WebSocketClientProtocol):
    def onMessage(self, payload, isBinary):
        dict_message = json.loads(payload)
        method_name = "{}{}".format(EVENT_STARTSWITH, dict_message.get('type'))
        method = getattr(self, method_name, None)
        method(dict_message)

    def __getattribute__(self, item):
        if item.startswith(EVENT_STARTSWITH):
            return lambda msg: self.factory.run_deferred(item.split(EVENT_STARTSWITH)[1], msg=msg)
        if item == 'onOpen':
            return lambda: self.factory.run_deferred(item)
        if item == 'onClose':
            return lambda wasClean, code, reason: self.factory.run_deferred(item)
        return object.__getattribute__(self, item)


class IKRTestFactory(WebSocketClientFactory):
    protocol = IKRTestProtocol

    def __init__(self, *args, **kwargs):
        super(IKRTestFactory, self).__init__(*args, **kwargs)
        self._deferreds = {}

    def on(self, event, callback=passthru, errback=passthru,
           callbackArgs=None, callbackKeywords=None,
           errbackArgs=None, errbackKeywords=None):
        if not event in self._deferreds:
            self._deferreds[event] = Deferred()
        defer = self._deferreds.get(event)
        defer.addCallbacks(callback, errback, callbackArgs, callbackKeywords, errbackArgs, errbackKeywords)
        return defer

    def buildProtocol(self, addr):
        self.client = super(IKRTestFactory, self).buildProtocol(addr)
        return self.client

    def run_deferred(self, deferred_type, msg=None):
        deferred = self._deferreds.get(deferred_type, None)
        if deferred and deferred.callbacks and not deferred.called:
            self._deferreds.pop(deferred_type)
            deferred.callback(msg)
