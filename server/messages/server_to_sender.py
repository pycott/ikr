# coding=utf-8
from schematics.types import StringType, DateTimeType, IntType, BaseType
from schematics.types.compound import ListType, DictType

from commons.validating import RbtSchematicsModel
from server.messages import USERNAME_MIN_LENGTH


class ConfirmMessage(RbtSchematicsModel):
    _type = 'confirm_message'
    uuid = StringType(required=True)
    datetime = DateTimeType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class ConfirmMessageRoom(RbtSchematicsModel):
    _type = 'confirm_message_room'
    uuid = StringType(required=True)
    datetime = DateTimeType(required=True)
    room_id = IntType(required=True)


class CreateRoom(RbtSchematicsModel):
    _type = 'create_room'
    uuid = StringType(required=True)
    room_id = IntType(required=True)
    usernames = ListType(StringType(required=True, min_length=USERNAME_MIN_LENGTH), required=True)
    room_name = StringType(required=True)


class History(RbtSchematicsModel):
    _type = 'history'
    history = DictType(BaseType(), required=True)
