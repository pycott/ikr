# coding=utf-8
from schematics.types import StringType, IntType
from schematics.types.compound import DictType

from commons.validating import RbtSchematicsModel
from server import StatusEnum
from server.messages import USERNAME_MIN_LENGTH


class OnlineList(RbtSchematicsModel):
    _type = 'online_list'
    users = DictType(IntType(required=True, choices=StatusEnum.statuses))


class Error(RbtSchematicsModel):
    _type = 'error'
    message = StringType(required=True)
    exc_value = StringType(required=True)


class Status(RbtSchematicsModel):
    _type = 'status'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    status = IntType(required=True, choices=StatusEnum.statuses)
