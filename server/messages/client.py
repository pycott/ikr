# coding=utf-8
from schematics.types import StringType, IntType
from schematics.types.compound import ListType

from commons.validating import RbtSchematicsModel
from server import StatusEnum
from server.messages import USERNAME_MIN_LENGTH


class Status(RbtSchematicsModel):
    _type = 'status'
    status = IntType(required=True, choices=StatusEnum.statuses)


class Message(RbtSchematicsModel):
    _type = 'message'
    uuid = StringType(required=True)
    message = StringType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class CreateRoom(RbtSchematicsModel):
    _type = 'create_room'
    uuid = StringType(required=True)
    usernames = ListType(StringType(required=True, min_length=USERNAME_MIN_LENGTH), required=True)
    room_name = StringType()


class InviteToRoom(RbtSchematicsModel):
    _type = 'invite_to_room'
    usernames = ListType(StringType(required=True, min_length=USERNAME_MIN_LENGTH), required=True)
    room_id = IntType(required=True)


class LeaveRoom(RbtSchematicsModel):
    _type = 'leave_room'
    room_id = IntType(required=True)


class MessageRoom(RbtSchematicsModel):
    _type = 'message_room'
    uuid = StringType(required=True)
    message = StringType(required=True)
    room_id = IntType(required=True)


class CallOffer(RbtSchematicsModel):
    _type = 'call_offer'
    offer = StringType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    room_id = IntType(serialize_when_none=False)


class CallAnswer(RbtSchematicsModel):
    _type = 'call_answer'
    answer = StringType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    room_id = IntType(serialize_when_none=False)


class CallIce(RbtSchematicsModel):
    _type = 'call_ice'
    ice = StringType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    room_id = IntType(serialize_when_none=False)


class Call(RbtSchematicsModel):
    _type = 'call'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class TakeCall(RbtSchematicsModel):
    _type = 'take_call'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class CancelCall(RbtSchematicsModel):
    _type = 'cancel_call'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class Hangup(RbtSchematicsModel):
    _type = 'hangup'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class CallRoom(RbtSchematicsModel):
    _type = 'call_room'
    room_id = IntType(required=True)


class TakeCallRoom(RbtSchematicsModel):
    _type = 'take_call_room'
    room_id = IntType(required=True)


class CancelCallRoom(RbtSchematicsModel):
    _type = 'cancel_call_room'
    room_id = IntType(required=True)


class HangupRoom(RbtSchematicsModel):
    _type = 'hangup_room'
    room_id = IntType(required=True)


class History(RbtSchematicsModel):
    _type = 'history'
    limit = IntType(default=None)
    offset = IntType(default=0)
