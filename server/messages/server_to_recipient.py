# coding=utf-8
from schematics.transforms import blacklist
from schematics.types import StringType, DateTimeType, IntType
from schematics.types.compound import ListType

from commons.validating import RbtSchematicsModel
from server.messages import USERNAME_MIN_LENGTH


class Message(RbtSchematicsModel):
    _type = 'message'
    message = StringType(required=True)
    datetime = DateTimeType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class MessageRoom(RbtSchematicsModel):
    _type = 'message_room'
    message = StringType(required=True)
    datetime = DateTimeType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    room_id = IntType(required=True)

    class Options:
        roles = {'history': blacklist('room_id', 'type')}


class InviteToRoom(RbtSchematicsModel):
    _type = 'invite_to_room'
    room_id = IntType(required=True)
    usernames = ListType(StringType(required=True, min_length=USERNAME_MIN_LENGTH), required=True)
    room_name = StringType(required=True)


class InvitedIntoRoom(RbtSchematicsModel):
    _type = 'invited_into_room'
    room_id = IntType(required=True)
    usernames = ListType(StringType(required=True, min_length=USERNAME_MIN_LENGTH), required=True)


class LeaveRoom(RbtSchematicsModel):
    _type = 'leave_room'
    room_id = IntType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class CallOffer(RbtSchematicsModel):
    _type = 'call_offer'
    offer = StringType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    room_id = IntType(serialize_when_none=False)


class CallAnswer(RbtSchematicsModel):
    _type = 'call_answer'
    answer = StringType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    room_id = IntType(serialize_when_none=False)


class CallIce(RbtSchematicsModel):
    _type = 'call_ice'
    ice = StringType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
    room_id = IntType(serialize_when_none=False)


class Call(RbtSchematicsModel):
    _type = 'call'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class TakeCall(RbtSchematicsModel):
    _type = 'take_call'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class CancelCall(RbtSchematicsModel):
    _type = 'cancel_call'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class Hangup(RbtSchematicsModel):
    _type = 'hangup'
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class CallRoom(RbtSchematicsModel):
    _type = 'call_room'
    room_id = IntType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class TakeCallRoom(RbtSchematicsModel):
    _type = 'take_call_room'
    room_id = IntType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class CancelCallRoom(RbtSchematicsModel):
    _type = 'cancel_call_room'
    room_id = IntType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)


class HangupRoom(RbtSchematicsModel):
    _type = 'hangup_room'
    room_id = IntType(required=True)
    username = StringType(required=True, min_length=USERNAME_MIN_LENGTH)
