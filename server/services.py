# coding=utf-8

from commons.exceptions import RoomDoesNotExist
from server.messages import ROOM_NAME_SEPARATOR
from server.models import Room


class IKRRoomService(object):
    counter = 0

    def __init__(self):
        self._reserved_room_ids = []
        self._rooms = {}

    def get_new_room_id(self):
        self.counter += 1
        room_id = self.counter
        return room_id

    def get_room(self, id):
        try:
            return self._rooms[id]
        except KeyError:
            raise RoomDoesNotExist

    def create_room(self, usernames, room_name=None):
        room_id = self.get_new_room_id()
        room_name = room_name or ROOM_NAME_SEPARATOR.join(usernames)
        self._rooms[room_id] = Room(room_id, room_name, usernames)
        return room_id, room_name
