# coding=utf-8
import sys

from auth.checkers import DebugBasicAuthorizationChecker

DEBUG = True
LOG_OUTPUT = sys.stdout

DEBUG_USER_PREFIX = 'user'
DEBUG_USERS_AMOUNT = 10
DEBUG_USER_PASSWORD = '123123'
ADDITIONAL_DEBUG_USERS = ['user1', 'user2']

DEBUG_USERS = {
    DEBUG_USER_PREFIX + str(i): DEBUG_USER_PASSWORD
    for i in range(1, DEBUG_USERS_AMOUNT + 1)
}

for username in ADDITIONAL_DEBUG_USERS:
    DEBUG_USERS[username] = DEBUG_USER_PASSWORD

AUTH_CHECKERS = (
    DebugBasicAuthorizationChecker(DEBUG_USERS),
)
