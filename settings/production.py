# coding=utf-8
import authkerb
from twisted.python.logfile import DailyLogFile

from settings.main import KRB5_SERVICE, KRB5_REALM


LOG_OUTPUT = DailyLogFile.fromFullPath("/var/log/twd.log")

AUTH_CHECKERS = (
    authkerb.NegotiateCredentialsChecker(),
    authkerb.BasicCredentialsChecker(KRB5_SERVICE, KRB5_REALM)
)
