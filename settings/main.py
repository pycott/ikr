# coding=utf-8

import os

from twisted.internet.base import DelayedCall
from twisted.internet.defer import setDebugging

DEBUG = False
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

WS_PORT = 9080
WSS_PORT = 9443
INTERFACE = '0.0.0.0'

USE_WS = True
USE_WSS = True

SSL_PRIVATE_CRT = 'keys/ikr.key'
SSL_PUBLIC_CRT = 'keys/ikr.crt'

HISTORY_BUFFER_FILE = os.path.join(BASE_DIR, 'history_buffer.shv')

KRB5_REALM = "VAGSH.LOCAL"
KRB5_SERVICE = "HTTP/ikr.vagsh.local"
os.environ['KRB5_KTNAME'] = '/etc/krb5.keytab'

if os.environ.get('TWISTED_ENV') == 'dev':
    from develop import *
else:
    from production import *

try:
    from local import *
except ImportError:
    pass

setDebugging(DEBUG)
DelayedCall.debug = DEBUG
